package com.company;

public class DivisionByZeroException extends Exception{
    String s;
    public DivisionByZeroException(){
        s = "Division by zero";
    }
    @Override
    public String getMessage(){
        return s;
    }
}
