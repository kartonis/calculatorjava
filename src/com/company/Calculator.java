package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

public class Calculator {
    private static final Logger logger = Logger.getGlobal();
    private Stack <Double> stack = new Stack<>();
    private Map <String, Calculation> operations = new TreeMap<>();
    private Map <String, Double> bijection = new TreeMap<>();
    public Calculator() {
        try {
            File in = new File("operations.txt");
            Scanner scanner = new Scanner(in);
            while (scanner.hasNextLine()) {
                String s = scanner.nextLine();
                String[] array = s.split(" ");
                if (array.length == 2){
                    try {
                        operations.put(array[0], (Calculation)Class.forName(array[1]).newInstance());
                    }
                    catch (Exception ex)
                    {
                        logger.info(ex.getMessage());
                    }
                }
            }
        }
        catch (FileNotFoundException ex)
        {
            logger.info(ex.getMessage());
        }

    }
    public void computer(String s) {
        String[] array;
        array = s.split(" ");
        if (array[0].equals("DEFINE")) {
            logger.info("Operation " + array[0]);
            bijection.put(array[1], Double.parseDouble(array[2]));
        }
        else if (array[0].equals("PUSH")) {
            if (bijection.containsKey(array[1])) {
                logger.info("Operation " + array[0]);
                stack.push(bijection.get(array[1]));
            } else stack.push(Double.parseDouble(array[1]));
        }
        else if (operations.containsKey(array[0])){
            logger.info("Operation " + array[0]);
            try {
                operations.get(array[0]).compute(stack);
            }
            catch (DivisionByZeroException e){
                System.out.println(e.getMessage());
            }
            catch (NegativeNumberException e){
                System.out.println(e.getMessage());
            }
        }
        else System.out.println("Invalid command");
    }
}
