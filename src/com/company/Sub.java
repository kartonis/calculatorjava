package com.company;

import java.util.Stack;

public class Sub extends Calculation {
    public void compute(Stack<Double> stack){
        double a = stack.pop();
        double b = stack.pop();
        stack.add(a - b);
    }
}

