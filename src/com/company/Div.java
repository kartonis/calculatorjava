package com.company;

import java.util.Stack;

public class Div extends Calculation {
    public void compute(Stack<Double> stack) throws DivisionByZeroException {
        double a = stack.pop();
        double b = stack.pop();
        if (a != 0)
            stack.add(b / a);
        else throw new DivisionByZeroException();
    }
}

