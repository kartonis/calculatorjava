package com.company;

import java.util.Stack;

public class Mul extends Calculation {
    public void compute(Stack<Double> stack){
        double a = stack.pop();
        double b = stack.pop();
        stack.add(a * b);
    }
}

