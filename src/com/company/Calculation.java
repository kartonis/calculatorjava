package com.company;

import java.util.Stack;

public abstract class Calculation {
    public abstract void compute(Stack<Double> stack) throws DivisionByZeroException, NegativeNumberException;
}
