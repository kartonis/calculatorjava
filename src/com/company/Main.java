package com.company;

import java.util.Scanner;
import java.io.*;


public class Main {
    public static void main(String[] args) {
        String line;
        Scanner scanner;
        if (args[0]!=null) {
            try {
                File input = new File(args[0]);
                scanner = new Scanner(input);
            }
            catch (FileNotFoundException e)
            {
                System.out.println("File not found\n");
                scanner = new Scanner(System.in);
            }
        }
        else {
            scanner = new Scanner(System.in);
        }
        Calculator split = new Calculator();
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            split.computer(line);
        }
        scanner.close();
        return;
    }
}
