package com.company;

public class NegativeNumberException extends Exception {
    String s;
    public NegativeNumberException(){
        s = "Extracting a root from a negative number";
    }
    @Override
    public String getMessage(){ return s; }
}
