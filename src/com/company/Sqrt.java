package com.company;
import java.lang.Math;
import java.util.Stack;

public class Sqrt extends Calculation {
    public void compute(Stack<Double> stack) throws NegativeNumberException {
        if(stack.peek() > 0)
            stack.add(Math.sqrt(stack.pop()));
        else throw new NegativeNumberException();
    }
}

