package Tests;

import com.company.Calculator;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class WrongInput {

    @Test
    void compute() {
        Calculator ob = new Calculator();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos) ;
        PrintStream old = System.out;
        System.setOut(ps);
        ob.computer("PASH A");
        String res = new String(baos.toByteArray());
        assertTrue(res.equals("Invalid command\n"));
        System.out.flush();
        System.setOut(old);
    }
}