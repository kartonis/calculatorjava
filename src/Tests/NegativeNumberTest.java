package Tests;

import com.company.Calculator;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class NegativeNumberTest {

    @Test
    void compute() {
        Calculator ob = new Calculator();
        ob.computer("PUSH -5");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos) ;
        PrintStream old = System.out;
        System.setOut(ps);
        ob.computer("SQRT");
        String res = new String(baos.toByteArray());
        assertTrue(res.equals("Extracting a root from a negative number\n"));
        System.out.flush();
        System.setOut(old);
    }
}