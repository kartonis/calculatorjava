package Tests;

import com.company.Calculator;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class DivisionByZeroTest {

    @Test
    void compute() {
        Calculator ob = new Calculator();
        ob.computer("PUSH 90");
        ob.computer("PUSH 0");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos) ;
        PrintStream old = System.out;
        System.setOut(ps);
        ob.computer("/");
        String res = new String(baos.toByteArray());
        assertTrue(res.equals("Division by zero\n"));
        System.out.flush();
        System.setOut(old);
    }
}