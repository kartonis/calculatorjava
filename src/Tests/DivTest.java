package Tests;

import com.company.Calculator;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DivTest {

    @Test
    void compute() {
        Calculator ob = new Calculator();
        ob.computer("PUSH 36");
        ob.computer("PUSH 6");
        ob.computer("/");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos) ;
        PrintStream old = System.out;
        System.setOut(ps);
        ob.computer("PRINT");
        String res = new String(baos.toByteArray());
        assertEquals(Double.parseDouble(res), 6);
        System.out.flush();
        System.setOut(old);
    }
}